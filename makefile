# Makefile for symbol table framework.

# This makefile contains no information about file structure or tool locations.
# All such configurations should be made in makefile.inc

include makefile.inc

.ONESHELL:
.SUFFIXES:

# Artifact names:
export PRODUCT_NAME := Symbol Table
export Description := Symbol table module

# Command aliases:
export SHELL := /bin/sh
export JAVA := $(JAVA_HOME)/bin/java

# Relative locations:
export ThisDir := $(shell pwd)
export symboltable_jar := $(MVN_REPO)/com/cliffberg/symboltable/symboltable/$(VERSION)/symboltable-$(VERSION).jar
export test_jar := $(MVN_REPO)/com/cliffberg/symboltable/test/$(FLOW_VERSION)/test-$(VERSION).jar
export symboltable_classes := $(ThisDir)/symboltable/maven/classes
export test_classes := $(ThisDir)/test/maven/classes
export javadoc_dir := javadocs
export AntlrJar := $(AntlrDir)/antlr-4.7.1-complete.jar

# Aliases:
test := $(JAVA) -cp $(CUCUMBER_CLASSPATH):$(test_classes):$(parser_classes):$(AntlrJar)
test := $(test) cucumber.api.cli.Main --glue cliffberg.symboltable.test
test := $(test) test/features


################################################################################
# Tasks


.PHONY: all gen_config install symboltable test javadoc test_all cukehelp cukever clean info

all: gen_config install

mvnversion:
	$(MVN) --version


# ------------------------------------------------------------------------------
# Generate the Config class that the runtime uses to determine the version of DABL.
gen_config:
	echo "package cliffberg.symboltable;" > symboltable/java/cliffberg/symboltable/Config.java
	echo "public class Config {" >> symboltable/java/cliffberg/symboltable/Config.java
	echo "public static final String Version = \"$(VERSION)\";" >> symboltable/java/cliffberg/symboltable/Config.java
	echo "}" >> symboltable/java/cliffberg/symboltable/Config.java


# ------------------------------------------------------------------------------
# Compile everything.
install: gen_config
	$(MVN) clean install


# ------------------------------------------------------------------------------
# Compile the Generator.
symboltable:
	$(MVN) clean install --projects symboltable


# ------------------------------------------------------------------------------
# For development: Compile only the behavioral tests.
test:
	echo MVN=$(MVN)
	$(MVN) clean install --projects test


# ------------------------------------------------------------------------------
# Generate javadocs for all modules.
javadoc:
	$(MVN) javadoc:javadoc


# ------------------------------------------------------------------------------
# Run Cucumber tests.
#	Gherkin tags: done, smoke, notdone, docker, exec, unit, pushlocalrepo, task,
#	patternsets, inputsandoutputs

test_all:
	echo test=$(test)
	$(test) --tags @done

cukehelp:
	java -cp $(CUCUMBER_CLASSPATH) cucumber.api.cli.Main --help

cukever:
	java -cp $(CUCUMBER_CLASSPATH) cucumber.api.cli.Main --version


# ------------------------------------------------------------------------------
clean:
	$(MVN) clean

info:
	@echo "Makefile for $(PRODUCT_NAME)"
