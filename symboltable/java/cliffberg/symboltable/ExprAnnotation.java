package cliffberg.symboltable;

public class ExprAnnotation<Type extends ValueType, Node> implements Annotation
{
	private Node node;
	private Object value;  // may be null
	private TypeDescriptor<Type> typeDesc;
	
	public ExprAnnotation(Node node, Object value, TypeDescriptor<Type> typeDesc) {
		this.node = node;
		this.value = value;
		this.typeDesc = typeDesc;
	}
	
	public Node getNode() { return node; }
	
	/**
	 May return null.
	 */
	public Object getValue() { return value; }
	
	public TypeDescriptor<Type> getTypeDesc() { return typeDesc; }
	
	public Type getType() { return typeDesc.getType(); }
}
