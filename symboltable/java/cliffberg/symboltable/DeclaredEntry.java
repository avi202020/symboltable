package cliffberg.symboltable;

/**
 Annotation for an Id that is explicitly declared - i.e., appears in a declaration.
 */
public class DeclaredEntry<Type extends ValueType, Node, TId extends Node> extends SymbolEntry<Type, Node, TId>
{
	private Node definingNode;

	public DeclaredEntry(String name, NameScope<Type, Node, TId> enclosingScope, Node definingNode)
	{
		super(name, enclosingScope);
		this.definingNode = definingNode;
		if (definingNode == null) throw new RuntimeException();
	}

	public Node getDefiningNode() { return definingNode; }
}
