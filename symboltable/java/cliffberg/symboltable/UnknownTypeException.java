package cliffberg.symboltable;


public class UnknownTypeException extends Exception {
	
	private static final String Message = "The type is unknown";
	
	public UnknownTypeException() {
		super(Message);
	}
	
	public UnknownTypeException(Throwable cause) {
		super(Message, cause);
	}
}
