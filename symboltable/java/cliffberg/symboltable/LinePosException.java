package cliffberg.symboltable;

public class LinePosException extends RuntimeException {
	public LinePosException(String msg) {
		super(msg);
	}
	
	public LinePosException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
