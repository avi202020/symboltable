package cliffberg.symboltable;

public interface VisibilityChecker {
	/**
	 Check if the symbol entry is visible within the specified scope. Throw
	 a RuntimeException if it is not.
	 */
	void check(NameScope scope, SymbolEntry entry);

	/**
	 Return true if the entry is visible; false otherwise.
	 */
	boolean isVisible(NameScope scope, SymbolEntry entry);
}
